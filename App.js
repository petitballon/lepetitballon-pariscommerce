import React, { Component } from 'react';
import AppNavigator from './src/config/routes';

export default class App extends Component {
	render() {
		return <AppNavigator />;
	}
}