import { StyleSheet } from 'react-native';
import { APP_COLORS } from '../../../styles/colors';

export const style = StyleSheet.create({
    header: {
        paddingTop: 20,
        backgroundColor: APP_COLORS.White,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center'
    }
});