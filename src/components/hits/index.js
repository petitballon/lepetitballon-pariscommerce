import React from 'react';
// First, we need to import the FlatList and other React Native component
import { View, FlatList, Image, Text, TouchableOpacity } from 'react-native';
// We also need to add the connectInfiniteHits connector to our import
import { connectInfiniteHits } from 'react-instantsearch-native';
import { style } from './style';
const Hits = connectInfiniteHits(({ hits, hasMore, refine }) => {

  /* if there are still results, you can
  call the refine function to load more */
  const onEndReached = function() {
    if (hasMore) {
      refine();
    }
  };

  return (
    <FlatList
        numColumns={4}
        contentContainerStyle={style.product_flatlist}
        data={hits}
        onEndReached={onEndReached}
        keyExtractor={(item, index) => item.objectID}
        renderItem={({item}) => (
            <TouchableOpacity
                style={style.product_card}
                onPress={() => getProductInfo(item.id)} key={item.id}
            >
                <View>
                    <View style={style.product_center}>
                        <Image
                            style={style.product_image}
                            source={{ uri: item.image_url}}
                        />
                    </View>
                    <View style={style.product_content}>
                        <View>
                            <Text style={style.product_name}>{item.name}</Text>
                            {/* <Text style={style.product_winemaker}>{item.domain.name}</Text>
                            <Text style={style.product_aop}>{item.appellation.toUpperCase()}</Text> */}
                        </View>
                    </View>
                    {/* <View style={style.product_row}>
                        <Text style={[style.product_price, style.product_cadetGrey]}>{item.price} €</Text>
                        <Text style={style.product_price}>{item.subscriber_price} €</Text>
                    </View> */}
                    {/* <View style={[{backgroundColor: item.label[0].hex}, style.product_label]}>
                        <Text style={style.product_label_text}>{item.label[0].name}</Text>
                    </View> */}
                </View>
            </TouchableOpacity>
        )}
    />
    // <FlatList
    //   data={hits}
    //   onEndReached={onEndReached}
    //   keyExtractor={(item, index) => item.objectID}
    //   renderItem={({ item }) => {
    //     return (
    //       <View style={{ flexDirection: 'row', alignItems: 'center' }}>
    //         <Image
    //           style={{ height: 100, width: 100 }}
    //           source={{ uri: item.image }}
    //         />
    //         <View style={{ flex: 1 }}>
    //           <Text>
    //             {item.name}
    //           </Text>
    //           <Text>
    //             {item.type}
    //           </Text>
    //         </View>
    //       </View>
    //     );
    //   }}
    // />
  );
});

export default Hits;