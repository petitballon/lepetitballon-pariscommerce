import { StyleSheet, Dimensions } from 'react-native';
import { colors } from '../../config/colors';

export const style = StyleSheet.create({
    product_flatlist: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingLeft: 8,
        paddingRight: 8
    },
    product_card: {
        marginTop:10,
        marginBottom:10,
        marginLeft:5,
        marginRight:5,
        borderWidth: 1,
        borderColor: colors.GainsBoro,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        width: 242,
        backgroundColor: colors.White
    },
    product_center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    product_content: {
        paddingLeft: 10,
        paddingRight: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: 100
    },
    product_image: {
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 'auto',
        width: 140,
        height: 280,
        zIndex: 0
    },
    product_name: {
        fontSize: 14,
        lineHeight: 16,
        textAlign: 'center',
        color: colors.Charcoal,
        fontFamily: 'markpro-bold'
    },
    product_winemaker: {
        paddingTop: 4,
        paddingBottom: 4,
        fontSize: 13,
        lineHeight: 14,
        textAlign: 'center',
        fontFamily: 'markpro'
    },
    product_aop: {
        fontSize: 11,
        fontFamily: 'markpro-medium',
        lineHeight: 16,
        textAlign: 'center',
        color: colors.CadetGrey
    },
    product_product_desc: {
        fontSize: 18,
        fontStyle: 'italic',
        fontWeight: '200',
        lineHeight: 22,
        color: colors.Charcoal
    },
    product_entitled: {
        marginLeft: 20,
        marginRight: 20,
        paddingTop: 12,
        borderTopWidth: 1,
        borderTopColor: colors.GainsBoro,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignSelf: 'stretch'
    },
    product_priceText: {
        fontSize: 12,
        fontWeight: '500',
        lineHeight: 15
    },
    product_priceTextSubscriber: {
        fontWeight: '500',
    },
    product_row: {
        marginLeft: 20,
        marginRight: 20,
        paddingBottom: 12,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        alignSelf: 'stretch',
        paddingTop: 12,
        borderTopWidth: 1,
        borderTopColor: colors.GainsBoro
    },
    product_price: {
        fontSize: 18,
        fontFamily: 'markpro-bold',
        lineHeight: 20
    },
    product_label: {
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        height: 25,
        alignSelf: 'stretch',
        alignItems: 'center'
    },
    product_label_text: {
        textAlign: 'center',
        lineHeight: 24,
        color: colors.White,
        fontSize: 12,
        fontFamily: 'markpro-bold'
    },
    product_cadetGrey: {
        color: colors.CadetGrey
    }
});
