import React from 'react';
import _ from 'lodash';
import {
    View,
    Text
} from 'react-native';

import { Button } from 'react-native-elements';

import { colors } from '../../config/colors';
import { style } from './style';

const LabelDetail = ({ infoDetail, products }) => (
    <View style={style.info_detail_container}>
        <View style={style.info_detail_left}>
            <View style={[style.info_detail_leftBackground, {backgroundColor: infoDetail.color}]}>
                <View style={{justifyContent: 'center'}}>
                    <Text style={[style.info_detail_label, style.info_detail_white]}>{infoDetail.title}</Text>
                    <Text style={[style.info_detail_text, style.info_detail_white]}>{infoDetail.text}</Text>
                </View>
                <Button
                    title="Voir les vins"
                    textStyle={style.info_detail_button_text}
                    buttonStyle={style.info_detail_button}
                    color={colors.White}
                    onPress={() => products()}
                />
            </View>
        </View>
        <View style={style.info_detail_right}>
            <View style={[style.info_detail_rightBackground, {backgroundColor: infoDetail.alpha}]}>
                <View style={style.info_detail_desc}>
                    <Text style={style.info_detail_headline}>Force</Text>
                    <View style={[style.info_detail_bar, {backgroundColor: "rgba(255,255,255,0.3)"}]}>
                        <View style={{width: infoDetail.progress + "%", height: 10, backgroundColor: infoDetail.color}}></View>
                    </View>
                </View>
                <View style={style.info_detail_desc}>
                    <Text style={style.info_detail_headline}>Goûts</Text>
                    <View
                        style={style.info_detail_row}
                    >
                        {
                            _.values(infoDetail.tags).map((tag, index) => {
                                return (
                                    <View
                                        key={index}
                                        style={{backgroundColor: infoDetail.color, margin: 2}}
                                    >
                                        <Text style={style.info_detail_badge}>{tag.name}</Text>
                                    </View>
                                )
                            })
                        }
                    </View>
                </View>
                <View style={style.info_detail_desc}>
                    <Text style={style.info_detail_headline}>Régions emblématiques</Text>
                    <View
                        style={style.info_detail_row}>
                        {
                            _.values(infoDetail.regions).map((region, index) => {
                                return (
                                    <View
                                        key={index}
                                        style={{backgroundColor: infoDetail.color, margin: 2}}
                                    >
                                    <Text style={style.info_detail_badge}>{region.name}</Text>
                                    </View>
                                )
                            })
                        }
                    </View>
                </View>
                <View style={style.info_detail_desc}>
                    <Text style={style.info_detail_headline}>Au restaurant vous dîtes</Text>
                    <Text style={{fontFamily: 'markpro-bold', fontSize: 14, color: infoDetail.color}}>{infoDetail.restaurant}</Text>
                </View>
                <View style={style.info_detail_desc}>
                    <Text style={style.info_detail_headline}>Vins les plus connus</Text>
                    <View
                        style={style.info_detail_row}>
                        {
                            _.values(infoDetail.aop).map((i, index) => {
                                return (
                                    <View
                                        key={index}
                                        style={{backgroundColor: infoDetail.color, margin: 2}}
                                    >
                                        <Text style={style.info_detail_badge}>{i.name}</Text>
                                    </View>
                                )
                            })
                        }
                    </View>
                </View>
                <View style={style.info_detail_desc}>
                    <Text style={style.info_detail_headline}>Au supermarché</Text>
                    <Text style={{fontFamily: 'markpro-bold', fontSize: 14, color: infoDetail.color}}>{infoDetail.market}</Text>
                </View>
            </View>
        </View>
    </View>
);

export default LabelDetail;