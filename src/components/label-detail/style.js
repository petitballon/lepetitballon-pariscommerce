import { StyleSheet, Platform } from 'react-native';
import { colors } from '../../config/colors';

export const style = StyleSheet.create({
    info_detail_container: {
        flexDirection: 'row',
        flex:1
    },
    info_detail_left: {
        flex: 1
    },
    info_detail_leftBackground: {
        padding: 40,
        paddingTop: 160,
        height: "100%",
        alignItems: 'center'
    },
    info_detail_right: {
        flex: 2,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0,0.2)',
                shadowOffset: {height: 0, width: 1},
                shadowOpacity: 1,
                shadowRadius: 4
            },
            android: {
                elevation: 1
            }
        })
    },
    info_detail_rightBackground: {
        padding: 40,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'baseline',
        justifyContent: 'space-between',
        flex: 1
    },
    info_detail_headline: {
        marginBottom: 10,
        fontWeight: 'bold',
        fontSize: 14,
		fontFamily: 'markpro-bold',
        color: colors.Charcoal
    },
    info_detail_bar: {
        width: 288,
        height: 10,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0,0.2)',
                shadowOffset: {height: -1, width: 0},
                shadowOpacity: 2,
                shadowRadius: 0
            },
            android: {
                elevation: 1
            }
        })
    },
    info_detail_white: {
        color: colors.White
    },
    info_detail_button_text: {
        fontFamily: 'markpro-medium',
        fontSize: 18
    },
    info_detail_button: {
        borderWidth: 2,
        borderColor: colors.White,
        backgroundColor: "transparent",
        width: 230,
        height: 56
    },
    info_detail_desc: {
        width:288,
        marginTop:20,
        marginBottom:20
    },
    info_detail_label: {
        width: 230,
        fontSize: 42,
        fontFamily: 'markpro-black',
        lineHeight: 50
    },
    info_detail_text: {
        width: 230,
        marginTop: 10,
        marginBottom: 50,
        fontFamily: 'markpro',
        fontSize: 18,
        lineHeight: 26
    },
    info_detail_row: {
        flexWrap: 'wrap', 
        alignItems: 'flex-start',
        flexDirection:'row'
    },
    info_detail_badge: {
        margin: 5,
        fontSize: 14,
        fontFamily: 'markpro-medium',
        color: colors.White
    }
});
