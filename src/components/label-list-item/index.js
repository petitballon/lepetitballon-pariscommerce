import React from 'react';

import { View, TouchableOpacity } from 'react-native';

import { style } from './style';

const LabelListItem = (props) => (
    <TouchableOpacity onPress={() => props.action(props.attr)}>
        <View style={[props.active == props.index ? style.selectedStyle : style.normalStyle, {backgroundColor: props.color}]}></View>
    </TouchableOpacity>
);

export default LabelListItem;