import { StyleSheet, Platform } from 'react-native';

export const style = StyleSheet.create({
    normalStyle: {
        width: 44,
        height: 44,
        borderRadius: 44,
        marginLeft: 18,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0,0.30)',
                shadowOffset: {height: 1, width: 0},
                shadowOpacity: 1,
                shadowRadius: 2
            },
            android: {
                elevation: 1
            }
        })
    },
    selectedStyle: {
        width: 44,
        height: 44,
        borderRadius: 44,
        borderWidth: 2,
        borderColor: '#000',
        marginLeft: 18,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0,0.30)',
                shadowOffset: {height: 1, width: 0},
                shadowOpacity: 1,
                shadowRadius: 2
            },
            android: {
                elevation: 1
            }
        })
    }
});
