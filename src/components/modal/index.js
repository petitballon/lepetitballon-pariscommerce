import React from 'react';
import {
    View,
    Image,
    Text,
    ScrollView,
    TouchableOpacity
} from 'react-native';

import Modal from "react-native-modal";

import { style } from './style';

const ModalContainer = ({data, isVisible, close}) => (
    <Modal
        style={style.modal_container}
        isVisible={isVisible}
        animationIn="zoomInDown"
        animationOut="zoomOutDown"
    >
        <Image
            style={style.modal_image}
            source={{ uri: data.image }}
        />
        <ScrollView>
            <Text style={style.modal_domain}>{data.domain}</Text>
            <Text style={style.modal_name}>{data.name}</Text>
            <View style={style.modal_content}>
                <View style={{width: 244, paddingRight: 10}}>
                    <Text style={style.modal_title}>Dégustation</Text>
                    { data.occasion_perfect && <View style={style.modal_row}>
                        <Image style={{width:24, height: 24, marginRight: 6}} source={require('../../assets/images/modal/ic-glass.png')} />
                        <Text style={style.modal_text}>{data.occasion_perfect}</Text>
                    </View> }
                    { data.tasting_eye_text && <View style={style.modal_row}>
                        <Image style={{width:24, height: 24, marginRight: 6}} source={require('../../assets/images/modal/ic-eye.png')} />
                        <Text style={style.modal_text}>{data.tasting_eye_text}</Text>
                    </View> }
                    { data.tasting_mouth_text && <View style={style.modal_row}>
                        <Image style={{width:24, height: 24, marginRight: 6}} source={require('../../assets/images/modal/ic-mouth.png')} />
                        <Text style={style.modal_text}>{data.tasting_mouth_text}</Text>
                    </View> }
                    { data.tasting_nose_text && <View style={style.modal_row}>
                        <Image style={{width:24, height: 24, marginRight: 6}} source={require('../../assets/images/modal/ic-nose.png')} />
                        <Text style={style.modal_text}>{data.tasting_nose_text}</Text>
                    </View> }
                </View>
                <View style={{width: 244, paddingRight: 10}}>
                    <Text style={style.modal_title}>Côté fourchette</Text>
                    { data.agreement_perfect && <View style={style.modal_row}>
                        <Image style={{width:24, height: 24, marginRight: 6}} source={require('../../assets/images/modal/ic-fork.png')} />
                        <Text style={style.modal_text}>{data.agreement_perfect}</Text>
                    </View> }
                    { data.agreement_meat && <View style={style.modal_row}>
                        <Image style={{width:24, height: 24, marginRight: 6}} source={require('../../assets/images/modal/ic-meat.png')} />
                        <Text style={style.modal_text}>{data.agreement_meat}</Text>
                    </View> }
                    { data.agreement_cheese && <View style={style.modal_row}>
                        <Image style={{width:24, height: 24, marginRight: 6}} source={require('../../assets/images/modal/ic-cheese.png')} />
                        <Text style={style.modal_text}>{data.agreement_cheese}</Text>
                    </View> }
                    { data.agreement_fish && <View style={style.modal_row}>
                        <Image style={{width:24, height: 24, marginRight: 6}} source={require('../../assets/images/modal/ic-fish.png')} />
                        <Text style={style.modal_text}>{data.agreement_fish}</Text>
                    </View> }
                    { data.agreement_vegetables && <View style={style.modal_row}>
                        <Image style={{width:24, height: 24, marginRight: 6}} source={require('../../assets/images/modal/ic-vegetable.png')} />
                        <Text style={style.modal_text}>{data.agreement_vegetables}</Text>
                    </View> }
                    { data.agreement_dessert && <View style={style.modal_row}>
                        <Image style={{width:24, height: 24, marginRight: 6}} source={require('../../assets/images/modal/ic-dessert.png')} />
                        <Text style={style.modal_text}>{data.agreement_dessert}</Text>
                    </View> }
                </View>
            </View>
        </ScrollView>
        <TouchableOpacity style={style.modal_close} onPress={close}>
            <Image style={{width: 24, height: 24, marginRight: 6}} source={require('../../assets/images/modal/close.png')} />
        </TouchableOpacity>
    </Modal>
);

export default ModalContainer;