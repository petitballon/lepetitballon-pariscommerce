import { StyleSheet } from 'react-native';
import { colors } from '../../config/colors';

export const style = StyleSheet.create({
    modal_container: {
        position: 'relative',
		backgroundColor: colors.White,
        padding: 20,
        paddingTop: 40,
        borderRadius: 15,
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: "space-between",
		alignItems: "flex-start"
	},
	modal_image: {
		width: 279,
		height: 451
	},
	modal_content: {
        width: 540,
        flexDirection: 'row',
        alignItems: 'flex-start',
		justifyContent: "space-between",
    },
    modal_domain: {
        marginTop: 20,
        fontFamily: 'markpro-black',
        fontSize: 26,
        lineHeight: 32,
        color: colors.DeepCarmine
    },
    modal_name: {
        marginBottom: 40,
        fontFamily: 'markpro',
        fontSize: 32,
        lineHeight: 36,
        color: colors.Charcoal
    },
    modal_row: {
        marginTop: 12,
        marginBottom: 15,
        flexDirection: 'row',
		justifyContent: "flex-start",
    },
    modal_title: {
        marginLeft: 30,
        fontFamily: 'markpro-bold',
        fontSize: 16,
        color: colors.DeepCarmine
    },
	modal_text: {
        fontFamily: 'markpro',
		fontSize: 14,
        lineHeight: 18
    },
    modal_close: {
        position: 'absolute',
        top: 30,
        right: 30
    }
});