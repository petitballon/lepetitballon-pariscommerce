import React from 'react';
import _ from 'lodash';
import {
    View,
    Image,
    Text,
    FlatList,
    TouchableOpacity
} from 'react-native';
import { style } from './style';
import { connectHits } from 'react-instantsearch-native';

const Product = connectHits(({ getProductInfo, hits, hasMore, refine }) => {

    shouldComponentUpdate = () => {
        return false;
    }

    const onEndReached = function() {
        if (hasMore) { refine() }
    }
    
    return (
        <FlatList
            numColumns={4}
            contentContainerStyle={style.product_flatlist}
            data={hits}
            // onEndReached={onEndReached}
            // onEndReachedThreshold={4}
            initialNumToRender={4}
            maxToRenderPerBatch={4}
            keyExtractor={item => item.objectID}
            renderItem={({item}) => (
                <TouchableOpacity
                    style={style.product_card}
                    key={item.objectID}
                    onPress={() => getProductInfo(item.objectID)}
                >
                    <View>
                        <View style={style.product_center}>
                            <Image
                                style={style.product_image}
                                source={{
                                    uri: item.image,
                                    cache: 'force-cache'
                                }}
                            />
                        </View>
                        <View style={style.product_content}>
                            <View>
                                <Text style={style.product_name}>{item.name}</Text>
                                <Text style={style.product_winemaker}>{item.domain}</Text>
                                {item.appellation && <Text style={style.product_aop}>{item.appellation.toUpperCase()}</Text> }
                            </View>
                        </View>
                        <View style={style.product_row}>
                            <Text style={[style.product_price, style.product_cadetGrey]}>{item.prix_public}</Text>
                            <Text style={style.product_price}>{item.prix_subscriber}</Text>
                        </View>
                        <View style={[{backgroundColor: item.label_color}, style.product_label]}>
                            <Text style={style.product_label_text}>{item.label}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            )}
        />
    )
});

export default Product;