import React from 'react';
import {
	View,
	FlatList,
	Text,
	TouchableHighlight,
} from 'react-native';
import { connectRefinementList } from 'react-instantsearch-native';
import { style } from './style';
    
const RefinementList = connectRefinementList(({ refine, items }) =>
	<FlatList
		numColumns={6}
		contentContainerStyle={style.catalog_badge_container}
		data={items}
		keyExtractor={item => item.label}
		renderItem={({ item }) => {
			return (
				<View key={item.label}>
					<TouchableHighlight
						style={style.catalog_badge}
						onPress={() => { refine(item.value)}}
					>
						<Text style={item.isRefined ? { fontWeight: 'bold' } : {}}>
							{item.label}
						</Text>
					</TouchableHighlight>
				</View>
			);
		}}
	/>
);

export default RefinementList;

  