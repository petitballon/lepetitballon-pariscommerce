import { StyleSheet } from 'react-native';
import { colors } from '../../config/colors';

export const style = StyleSheet.create({
    catalog_badge_container: {
		// alignItems: 'center',
		paddingLeft: 6,
		paddingRight: 6
	},
	catalog_badge: {
		backgroundColor: colors.White,
		borderColor: colors.Charcoal,
		borderWidth: 1,
		borderRadius: 20,
		margin: 6,
		paddingTop: 6,
		paddingBottom: 6,
		paddingLeft: 12,
		paddingRight: 12
	},
	catalog_badge_label: {
		borderColor: 'transparent'
	}
});
