import { connectRefinementList } from 'react-instantsearch-native';
    
const VirtualRefinementList = connectRefinementList(() => null);

export default VirtualRefinementList;

  