import React from 'react';
import { createStackNavigator } from 'react-navigation';

import HomeScreen from '../screens/home';
import CatalogScreen from '../screens/catalog';
import FormulaScreen from '../screens/formula';
import LabelScreen from '../screens/label';
import QuizScreen from '../screens/quiz';
import ResultScreen from '../screens/result';

import Header from '../components/header';

const AppNavigator = createStackNavigator(
	{
		App: HomeScreen,
		Catalog: CatalogScreen,
		Label: LabelScreen,
		Formula: FormulaScreen,
		Result: ResultScreen,
		Quiz: QuizScreen,
	},
	{
		initialRouteName: "App",
		navigationOptions: {
			headerTitle: <Header/>
		}
	}
);

export default AppNavigator;