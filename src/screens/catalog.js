import React, { Component } from 'react';
import axios from 'axios';
import _ from 'lodash';
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity
} from 'react-native';
import { Button } from 'react-native-elements';
import { InstantSearch } from 'react-instantsearch-native';
import { connectCurrentRefinements, connectPagination } from "react-instantsearch/connectors";

/* var colors */
import { colors } from '../config/colors';
/* components */
import Product from '../components/product';
import RefinementList from '../components/refine/refinement-list';
import ModalContainer from '../components/modal';

const BASE_API_URL = 'https://www.lepetitballon.com/privateapi/v1/shop';

class CatalogScreen extends Component {

	static navigationOptions = ({ navigation }) => {
		return {
			headerRight: (
				<Button
					title="Filtrer"
					textStyle={styles.header_right_text}
					buttonStyle={styles.header_right_button}
					onPress={navigation.getParam('toggleFilter')}
				/>
			)
		}
	}

	componentDidMount = ()  => {
		this.props.navigation.setParams({ 'toggleFilter': this._toggleFilter });
	}

	state = {
		filteredLabel: this.props.navigation.getParam("label"),
		routeFrom: this.props.navigation.getParam("from"),
		productInfo: [],
		isModalVisible: false,
		isFilterOn: false
	}

	getProductInfo = (product_id) => {
		axios.get(
			`${BASE_API_URL}/info/${product_id}`,
			{
				headers: {
					'Authorization': 'token-from-auth-api',
					'TOKEN': 'MyTestToken',
				}
			}
		)
		.then((response) => {
			this.setState({
				productInfo: response.data
			}, () => {
				this._toggleModal();
			})
		})
	}

	_toggleFilter = () => this.setState({ isFilterOn: !this.state.isFilterOn });
	_toggleModal = () => this.setState({ isModalVisible: !this.state.isModalVisible });

	render() {

		const state = this.state;

		const ClearAllRefinements = ({ refine, items }) => (
			<Button
				title="Effacer les filtres"
				textStyle={styles.header_right_text}
				buttonStyle={styles.header_right_button}
				onPress={() => refine(items)}
			/>
		)

		const range = (start, end) => Array.from({ length: end - start + 1 }, (_, i) => start + i);

		const Pagination = ({ padding = 3, refine, currentRefinement, nbPages }) => (
			<View style={styles.pagination}>
			  {range(
				Math.max(1, currentRefinement - padding),
				Math.min(nbPages, currentRefinement + padding)
			  ).map(page => (
				<TouchableOpacity
				  key={page}
				  onPress={() => refine(page)}
				>
					<Text style={[currentRefinement === page ? styles.page_active : styles.page_inactive ]}>{page}</Text>
				</TouchableOpacity>
			  ))}
			</View>
		)
		  
		const ConnectedClearAllRefinements = connectCurrentRefinements(ClearAllRefinements);
		const ConnectedPagination = connectPagination(Pagination);

		const renderProducts = () => {

			return (
				<View style={styles.container}>
					<InstantSearch
						appId="RCRK58WE8U"
						apiKey="78ce454233839124bb68e3fb7085662c"
						indexName="lpb_rde"
					>
						<View style={[state.isFilterOn == false ? {display: 'none'} : {display: 'flex'}]}>
							<View>
								<ConnectedClearAllRefinements />
							</View>
							<View>
								<View style={styles.catalog_screen_row}>
									<Text style={styles.refine_title}>Je cherche un vin ?</Text>
									<RefinementList attribute="color" operator="and" />
								</View>
								<View style={styles.catalog_screen_row}>
									<Text style={styles.refine_title}>Qui vient de ?</Text>
									<RefinementList attribute="origin" operator="and" />
								</View>
								<View style={styles.catalog_screen_row}>
									<Text style={styles.refine_title}>Pour quelle occasion ?</Text>
									<RefinementList attribute="occasion" operator="and" />
								</View>
								<View style={styles.catalog_screen_row}>
									<Text style={styles.refine_title}>Label du Petit Ballon ?</Text>
									{
										state.routeFrom == 'label' ? <RefinementList attribute="label" operator="and" defaultRefinement={state.filteredLabel} /> : <RefinementList attribute="label" operator="and"/>
									}
								</View>
							</View>
						</View>
						<Product getProductInfo={this.getProductInfo} />
						<ConnectedPagination />
					</InstantSearch>
				</View>
			)

		}

		return (
			<View style={styles.catalog_screen_container}>
				<ScrollView>
					{ renderProducts() }
					<ModalContainer
						data={state.productInfo}
						isVisible={state.isModalVisible}
						close={this._toggleModal}
					/>
				</ScrollView>
			</View>
		)
	}
}

export default CatalogScreen;

const styles = StyleSheet.create({
	catalog_error_container: {
		position: 'relative',
		justifyContent: 'center',
		alignItems: 'center',
		height: 540,
		backgroundColor: colors.white
	},
	catalog_error_image: {
		width: 800,
		height: 385,
		margin: 'auto',
		position: 'absolute',
		bottom: 0
	},
	catalog_screen_container: {
		flex: 1,
		alignSelf: 'stretch',
		backgroundColor: colors.GhostWhite
	},
	catalog_screen_filters: {
		flexDirection:"row",
		flexWrap: "wrap",
		alignItems: 'center',
		justifyContent: 'flex-end'
	},
	catalog_screen_row: {
		flexWrap: 'wrap',
		paddingTop: 15,
		paddingLeft: 20,
		paddingRight: 20
	},
	refine_title: {
		fontFamily:"markpro-medium"
	},
	header_right_text: {
		fontFamily: 'markpro-bold',
		fontSize: 16,
		color: colors.Charcoal
	},
	header_right_button: {
		backgroundColor: 'transparent'
	},
	pagination: {
		margin: 20,
		paddingTop: 10,
		paddingLeft: 20,
		paddingRight: 20,
		alignItems:'center',
		flexDirection:'row',
		justifyContent: 'space-around',
		backgroundColor: colors.White
	},
	page_active: {
		width: 40,
		height: 40,
		fontSize: 18,
		fontFamily: 'markpro-bold',
		color: colors.BlueMunsell
	},
	page_inactive: {
		width: 40,
		height: 40,
		fontSize: 18,
		fontFamily: 'markpro-medium',
		color: colors.Charcoal
	}
});
