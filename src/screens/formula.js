import React, { Component } from 'react';
import _ from 'lodash';
import {
    StyleSheet,
    View,
    Platform,
    Text,
    Image
} from 'react-native';
import { Button } from 'react-native-elements';
/* var colors */
import { colors } from '../config/colors';

class FormulaScreen extends Component {

    render() {

        return (
            <View style={{flex:1}}>
                <View>
                    <Text style={styles.formula_screen_intro}>Pour offrir ou vous abonner, rendez-vous sur le site du Petit Ballon !</Text>
                </View>
                <View style={styles.formula_screen_container}>
                    <View style={styles.formula_screen_item}>
                        <Image
                            source={require('../assets/images/formula/product-adr.jpg')}
                            style={styles.formula_screen_image}
                        />
                        <Text style={[styles.formula_screen_title, {color: colors.EggBlue}]}>Âge de Raisin</Text>
                        <Text style={styles.formula_screen_headline}>La découverte par excellence</Text>
                        <Text style={styles.formula_screen_text}>Une sélection de Jean-Michel pour les amateurs privilégiant le rapport plaisir-qualité-prix et qui se réjouissent de dénicher des grands vins méconnus...</Text>
                        <View>
                            <View style={styles.formula_screen_row}>
                                <Text style={styles.formula_screen_desc}>Une sélection de 2 bouteilles pépites</Text>
                            </View>
                            <View style={styles.formula_screen_row}>
                                <Text style={styles.formula_screen_desc}>Des fiches vins pour apprendre à les déguster</Text>
                            </View>
                            <View style={styles.formula_screen_row}>
                                <Text style={styles.formula_screen_desc}>La Gazette du Petit Ballon pour tout savoir sur les vins</Text>
                            </View>
                        </View>
                        <Text style={[styles.formula_screen_price, {backgroundColor: colors.EggBlue}]}>A partir de 19,90 € / mois</Text>
                    </View>
                    <View style={styles.formula_screen_item}>
                        <Image
                            source={require('../assets/images/formula/product-bio.jpg')}
                            style={styles.formula_screen_image}
                        />
                        <Text style={[styles.formula_screen_title, {color: colors.LightGreen}]}>100% Bio</Text>
                        <Text style={styles.formula_screen_headline}>Pour déguster du beau, du bon, du BIO !</Text>
                        <Text style={styles.formula_screen_text}>L'occasion de redécouvrir le terroir français dans le respect de la terre et sa simplicité. Bref, c'est de la biodynamite!</Text>
                        <View>
                            <View style={styles.formula_screen_row}>
                                <Text style={styles.formula_screen_desc}>Une sélection de 2 bouteilles certifiées Bio</Text>
                            </View>
                            <View style={styles.formula_screen_row}>
                                <Text style={styles.formula_screen_desc}>Des fiches vins pour apprendre à les déguster</Text>
                            </View>
                            <View style={styles.formula_screen_row}>
                                <Text style={styles.formula_screen_desc}>La Gazette du Petit Ballon pour tout savoir sur les vins</Text>
                            </View>
                        </View>
                        <Text style={[styles.formula_screen_price, {backgroundColor: colors.LightGreen}]}>A partir de 22,90 € / mois</Text>
                    </View>
                    <View style={styles.formula_screen_item}>
                        <Image
                            source={require('../assets/images/formula/product-ba.jpg')}
                            style={styles.formula_screen_image}
                        />
                        <Text style={[styles.formula_screen_title, , {color: colors.LightSalmon}]}>Bel Âge</Text>
                        <Text style={styles.formula_screen_headline}>De grands vins pour connaisseurs</Text>
                        <Text style={styles.formula_screen_text}>Une sélection de Jean-Michel sur des domaines reconnus et des appellations plus recherchées. Une formule pour expérimenter, faites de crus renommés.</Text>
                        <View>
                            <View style={styles.formula_screen_row}>
                                <Text style={styles.formula_screen_desc}>Une sélection de 2 bouteilles de grande classe</Text>
                            </View>
                            <View style={styles.formula_screen_row}>
                                <Text style={styles.formula_screen_desc}>Des fiches vins pour apprendre à les déguster</Text>
                            </View>
                            <View style={styles.formula_screen_row}>
                                <Text style={styles.formula_screen_desc}>La Gazette du Petit Ballon pour tout savoir sur les vins</Text>
                            </View>
                        </View>
                        <Text style={[styles.formula_screen_price, {backgroundColor: colors.LightSalmon}]}>A partir de 39,90 € / mois</Text>
                    </View>
                </View>
            </View>
        )
    }
}

export default FormulaScreen;

const styles = StyleSheet.create({
    formula_screen_container: {
        position: 'relative',
        backgroundColor: colors.GhostWhite,
        padding: 20,
        flex:1,
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    formula_screen_item: {
        width: 315,
        padding: 15,
        backgroundColor: colors.White,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0,0.1)',
                shadowOffset: {height: 0, width: 1},
                shadowOpacity: 1,
                shadowRadius: 1
            },
            android: {
                elevation: 1
            }
        })
    },
    formula_screen_image: {
        width: 285,
        height: 220
    },
    formula_screen_title: {
        marginTop: 10,
        fontSize: 26,
        fontFamily: 'markpro-black'
    },
    formula_screen_headline: {
        marginTop: 10,
        marginBottom: 10,
        fontSize: 14,
        fontFamily: 'markpro-bold'
    },
    formula_screen_text: {
        marginBottom: 10,
        fontFamily: 'markpro',
        fontSize: 12,
        height: 75,
        lineHeight: 18
    },
    formula_screen_row: {
        borderTopWidth: 1,
        borderTopColor: colors.GainsBoro
    },
    formula_screen_row_last: {
        borderBottomWidth: 1,
        borderBottomColor: colors.GainsBoro,
    },
    formula_screen_desc: {
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 12,
        fontFamily: 'markpro-medium',
        fontStyle: 'italic',
    },
    formula_screen_intro: {
        marginTop: 12,
        marginBottom: 18,
        textAlign: 'center',
        fontSize: 28,
        fontFamily: 'markpro-black'
    },
    formula_screen_price: {
        paddingRight: 10,
        paddingBottom: 10,
        color: colors.White,
        backgroundColor: colors.Charcoal,
        textAlign: 'center',
        fontSize: 18,
        fontFamily: 'markpro-bold',
        height: 50,
        lineHeight: 46
    }
});