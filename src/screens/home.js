import React, { Component } from 'react';
import _ from 'lodash';
import { Font, ScreenOrientation } from 'expo';
import { Button } from 'react-native-elements';
import {
	StyleSheet,
	View,
	Image,
	TouchableHighlight
} from 'react-native';

/* var colors */
import { colors } from '../config/colors';

class HomeScreen extends Component {

	state = {
		fontLoaded: false
	}

	static navigationOptions = {
		header: null
	}

	async componentDidMount() {
		ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.LANDSCAPE_LEFT);

		await Font.loadAsync({
			'markpro': require('../assets/fonts/MarkPro.ttf'),
			'markpro-medium': require('../assets/fonts/MarkPro-Medium.ttf'),
			'markpro-bold': require('../assets/fonts/MarkPro-Bold.ttf'),
			'markpro-black': require('../assets/fonts/MarkPro-Black.ttf')
		});
		this.setState({ fontLoaded: true });
	}

	_goToScreen = (routeName, key) => {
		this.props.navigation.navigate({ routeName: routeName,
			key: key
		})
	}

	render() {

		const state = this.state;

		return (
			<View>
				<View style={styles.app_styles_top}>
					<View style={[styles.app_styles_block, {backgroundColor: colors.BlueMunsell}]}>
						<Image source={require('../assets/images/home/home-screen-1.jpg')} style={{width: 255, height: 170}} />
						<Button
							title="Je connais mon label"
							textStyle={state.fontLoaded == true ? styles.app_styles_button_text : null}
							buttonStyle={styles.app_styles_button}
							color={colors.White}
							onPress={() => this._goToScreen("Label", "label")}
						/>
					</View>
					<View style={[styles.app_styles_block, {backgroundColor: colors.EggBlue}]}>
						<Image source={require('../assets/images/home/home-screen-2.jpg')} style={{width: 255, height: 170}} />
						<Button
							title="Je découvre mon label"
							textStyle={state.fontLoaded == true ? styles.app_styles_button_text : null}
							buttonStyle={styles.app_styles_button}
							color={colors.White}
							onPress={() => this._goToScreen("Quiz", "quiz")}
						/>
					</View>
				</View>
				<View style={styles.app_styles_top}>
					<Image
						source={require('../assets/images/home/logo.png')}
						style={styles.app_styles_logo}
					/>
					<View style={[styles.app_styles_block, {backgroundColor: colors.Charcoal}]}>
						<Image source={require('../assets/images/home/home-screen-3.jpg')} style={{width: 255, height: 170}} />
						<Button
							title="Découvrir nos offres"
							textStyle={state.fontLoaded == true ? styles.app_styles_button_text : null}
							buttonStyle={styles.app_styles_button}
							color={colors.White}
							onPress={() => this._goToScreen("Formula", "formula")}
						/>
					</View>
					<View style={[styles.app_styles_block, {backgroundColor: colors.LightSalmon}]}>
						<Image source={require('../assets/images/home/home-screen-4.jpg')} style={{width: 255, height: 170}} />
						<Button
							title="Voir tous les vins"
							textStyle={state.fontLoaded == true ? styles.app_styles_button_text : null}
							buttonStyle={styles.app_styles_button}
							color={colors.White}
							onPress={() => this._goToScreen("Catalog", "catalog")}
						/>
					</View>
				</View>
				{/* <View>
					<View style={styles.app_styles_bottom}>
						<Image
							source={require('../assets/images/home/logo.png')}
							style={styles.app_styles_logo}
						/>
						<Image
							source={require('../assets/images/home/home-screen-3.jpg')}
							style={{width: 1024, height: 384}}
						/>
						<TouchableHighlight
							style={{position: "absolute", top: 100}}>
							<Button
								title="Voir tous les vins"
								textStyle={state.fontLoaded == true ? styles.app_styles_button_text : null}
								buttonStyle={styles.app_styles_button}
								color={colors.White}
								onPress={() => this._goToScreen("Catalog", "catalog")}
							/>
						</TouchableHighlight>
					</View>
				</View> */}
			</View>
		)
	}
}

export default HomeScreen;

const styles = StyleSheet.create({
	app_styles_logo: {
		width: 180,
		height: 179,
		position:"absolute",
		top: -100,
		zIndex:10
	},
	app_styles_top: {
		justifyContent: 'center',
		flexDirection: 'row',
		height: 384
	},
	app_styles_block: {
		width:"50%",
		height: 384,
		alignItems: 'center',
		justifyContent: 'center'
	},
	app_styles_bottom: {
		width: 1024,
		height: 384,
		position: "relative",
		alignItems:"center",
		justifyContent:"center"
	},
	app_styles_button_text: {
        fontFamily: 'markpro-medium',
        fontSize: 18
    },
	app_styles_button: {
		borderWidth: 2,
		borderColor: colors.White,
		backgroundColor: "transparent",
		marginTop: 30,
		padding: 10,
		width: 255,
		height: 56
	}
});