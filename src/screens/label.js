import React, { Component } from 'react';
import _ from 'lodash';
import {
    StyleSheet,
    View,
    Platform,
    FlatList,
    Text
} from 'react-native';
// Data
import labelsInfo from '../datas/labels-info';
import labelsCategory from '../datas/labels-category';
// Components
import LabelListItem from '../components/label-list-item';
import LabelDetail from '../components/label-detail';
/* var colors */
import { colors } from '../config/colors';

class LabelScreen extends Component {

    state = {
        labels: _.values(labelsInfo),
        currentLabel: _.values(labelsInfo).filter(l => l.title == "Le Canaille"),
        filteredLabel: this.props.navigation.getParam("label"),
		routeFrom: this.props.navigation.getParam("from")
    }

    componentDidMount = () => {
        const keys = this.props.navigation.state.key;

        if (keys === 'result') {
            this._updateCurrentLabel(this.state.filteredLabel);
        } else {
            this._updateCurrentLabel("Le Tendre");
        }
    }

    _updateCurrentLabel = (title) => {
        let selectedLabel = title;
        this.setState({
            currentLabel: this.state.labels.find(l => l.title == selectedLabel),
            selectedItem: selectedLabel
        })
    }

    _keyExtractor = (item) => item.id;

    _renderItem = ({item, index}) => {
        return (
            <LabelListItem
                active={this.state.selectedItem}
                color={item.color}
                attr={item.title}
                index={index}
                action={this._updateCurrentLabel.bind(this)}
            />
        )
    }

    _rendercat = () => {
        return (
            labelsCategory.map((cat, index) => {
                return (
                    <View key={index} style={[(index === 0) ? "" : styles.label_screen_cat_item, {width: cat.width}]}>
                        <Text style={[styles.label_screen_cat_text, {color: cat.color}]}>{cat.text.toUpperCase()}</Text>
                    </View>
                )
            })
        )
    }

    _goToCatalog = () => {
        this.props.navigation.navigate({
            routeName: 'Catalog',
            params: {
                label: [this.state.currentLabel.title],
                from: 'label'
            }
        })
    }

    render() {

        return (
            <View style={styles.label_screen_container}>
                <LabelDetail infoDetail={this.state.currentLabel} products={this._goToCatalog} />
                <View style={styles.label_screen_list}>
                    <View style={{height: 100}}>
                        <FlatList
                            numColumns={15}
                            contentContainerStyle={styles.label_screen_flatlist}
                            data={this.state.labels}
                            renderItem={this._renderItem}
                            keyExtractor={this._keyExtractor}
                        />
                    </View>
                    <View style={styles.label_screen_cat}>
                        { this._rendercat() }
                    </View>
                </View>
            </View>
        )
    }
}

export default LabelScreen;

const styles = StyleSheet.create({
    label_screen_container: {
        position: 'relative',
        flex:1,
    },
    label_screen_list: {
        position: 'absolute',
        bottom: 0,
        left: 20,
        right: 20,
        height: 145,
        padding: 0,
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        backgroundColor: colors.White,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0,0.2)',
                shadowOffset: {height: 0, width: 1},
                shadowOpacity: 1,
                shadowRadius: 4
            },
            android: {
                elevation: 1
            }
        })
    },
    label_screen_flatlist: {
        marginTop: 25,
        marginBottom: 20,
        paddingLeft: 20,
        paddingRight: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        flex: 1
    },
    label_screen_cat: {
        paddingLeft: 35,
        paddingRight: 35,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    label_screen_cat_item: {
        borderLeftWidth: 1,
        borderLeftColor: colors.CadetGrey
    },
    label_screen_cat_text: {
        fontFamily: 'markpro-medium',
        fontSize: 12,
        textAlign: 'center'
    }
});