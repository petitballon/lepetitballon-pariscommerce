import React, { Component } from 'react';
import axios from 'axios';
import _ from 'lodash';
import { Button } from 'react-native-elements';
import {
	StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity
} from 'react-native';
/* var colors */
import { colors } from '../config/colors';
import quizData from '../datas/quiz';

const staticResult = {
    "1_1": 1,
    "1_2": 1,
    "2": 1,
    "3": 1,
    "4": 1,
    "5": 1,
    "6": 1,
    "7_1": 1,
    "7_2": 1
}

const BASE_API_URL = 'https://www.lepetitballon.com/privateapi/v1/shop';

class quizScreen extends Component {

	state = {
        quizData: _.values(quizData),
        currentQuestion: _.values(quizData)[0],
        resultQuiz: [],
        count: 1,
        selectedItem: 1,
        result: staticResult
    }

    componentDidMount = () => {
        axios.post(
            `${BASE_API_URL}/quiz`, JSON.stringify(this.state.result)
		)
		.then((response) => {
            this.setState({
                resultQuiz: [response.data]
            })
        })
    }

    _fetchDiscovery = () => {

        axios.post(
            `${BASE_API_URL}/quiz`, JSON.stringify(this.state.result)
		)
		.then((response) => {
            this.setState({
                resultQuiz: [response.data]
            }, () => {
                this.props.navigation.navigate({ routeName: 'Result', params: {
                    resultQuiz: {...this.state.resultQuiz}
                }})
            })
        })
        
    }
    
    _getValue = (id, value) => {
        this.setState({
            result: { ...this.state.result, [id]: parseInt(value) },
            selectedItem: value
        })
    }

    _updateCurrentQuestion(count) {
        this.setState({
            count: this.state.count + 1
        }, () => {
            this.setState({
                currentQuestion: _.values(quizData)[count],
                selectedItem: 1
            })
        })
    }

    _renderItem = (id) => {

        const currentQ = this.state.currentQuestion;
        let count = this.state.count;
        
        return (
            currentQ.options.map((o, index) => {
                return (
                    <TouchableOpacity key={index} onPress={() => this._getValue(id, o.value)} style={styles.quiz_item}>
                        <View style={[this.state.selectedItem == o.value ? styles.quiz_selected : styles.quiz_unselected]}>
                            {(count >= 3) &&
                                <View style={styles.quiz_wrap_image}>
                                    <Image style={styles.quiz_image} source={{uri: o.image}} />
                                </View>
                            }
                            <View style={[count >= 3 ? '' : styles.quiz_wrap_image]}>
                                <Text value={o.value} style={[count >= 3 ? styles.quiz_name : styles.quiz_name_noimage]}>{o.name}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                )
            })
        )
    }

    _renderButton = () => {

        const state = this.state,
              count = state.count;

        if (count === 7 ) {
            return (
                <View style={styles.quiz_button_position}>
                    <Button
                        title="Découvrir votre label"
                        textStyle={styles.quiz_button_text}
                        buttonStyle={styles.quiz_button}
                        color={colors.White}
                        onPress={() => this._fetchDiscovery()}
                    />
                </View>
            )
        } else {
            return (
                <View style={styles.quiz_button_position}>
                    <Button
                        title="Question suivante"
                        textStyle={styles.quiz_button_text}
                        buttonStyle={styles.quiz_button}
                        color={colors.White}
                        onPress={() => this._updateCurrentQuestion(state.count)}
                    />
                </View>
            )
        }
    }

	render() {
        const state = this.state,
              question = state.currentQuestion;

		return (
            <View style={styles.quiz_container}>
                <View style={styles.quiz_body}>
                    <View style={styles.quiz_left}>
                        <Text style={styles.quiz_question}>{question.question}</Text>
                        <Text style={styles.quiz_text}>{question.text}</Text>
                    </View>
                    <View style={styles.quiz_right}>
                        <View style={{flexDirection:'row', flexWrap:'wrap', justifyContent:'center'}}>
                            { this._renderItem(question.id) }
                        </View>
                        { this._renderButton() }
                    </View>
                    
                </View>
            </View>
        )
	}
}

export default quizScreen;

const styles = StyleSheet.create({
    quiz_container: {
        flex: 1,
        flexDirection:'row',
        backgroundColor: colors.Charcoal
    },
    quiz_body: {
        flexDirection: 'row',
        flex: 1
    },
    quiz_left: {
        flex: 1,
        height: "100%",
        justifyContent:'center',
        padding: 40
    },
    quiz_right: {
        flex: 2,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        height: "100%",
        backgroundColor: colors.EggBlue,
    },
    quiz_selected:{
        opacity: 1
    },
    quiz_unselected:{
        opacity: 0.5
    },
    quiz_item: {
        margin: 10,
        flexWrap: 'wrap',
        width: 130
    },
    quiz_item_selected: {
        margin: 10,
        flexWrap: 'wrap',
        width: 130,
        opacity: 0.3
    },
    quiz_wrap_image: {
        width: 130,
        height: 130,
        borderRadius: 130/2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.White
    },
    quiz_image: {
        width: 110,
        height: 110
    },
    quiz_question: {
        fontFamily: 'markpro-black',
        fontSize: 26,
        lineHeight: 26,
        color: colors.White
    },
    quiz_text: {
        fontSize: 16,
        fontFamily: 'markpro-medium',
        color: colors.White
    },
    quiz_name: {
        marginTop: 20,
        textAlign: 'center',
        fontSize: 14,
        fontFamily: 'markpro-bold',
        color: colors.White
    },
    quiz_name_noimage: {
        textAlign: 'center',
        fontSize: 14,
        fontFamily: 'markpro-bold',
        color: colors.Charcoal
    },
    quiz_button: {
        padding: 10,
		width: 255,
		height: 56,
        backgroundColor: colors.DeepCarmine
    },
    quiz_button_position: {
        position:'absolute',
        bottom: 30,
        left: "50%",
        marginLeft: -130
    },
    quiz_button_text: {
        fontFamily: 'markpro-bold',
        fontSize: 18,
        color: colors.White
    }
});