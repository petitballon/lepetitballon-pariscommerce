import React, { Component } from 'react';
import _ from 'lodash';
import {
    StyleSheet,
    View,
    Platform,
    Text,
    Image
} from 'react-native';

import { Button } from 'react-native-elements';
import { colors } from '../config/colors';

import labelsInfo from '../datas/labels-info';

class ResultScreen extends Component {

    // static navigationOptions = () => {
	// 	return {
	// 		headerLeft: null
	// 	}
	// }

    state = {
        paramQuiz: _.values(this.props.navigation.getParam("resultQuiz")),
        labels: _.values(labelsInfo),
        userLabel: [],
        userQuizValue: ""
    }

    componentDidMount = () => {
        this.setState(state => ({
            userQuizValue: state.paramQuiz[0].label_red
        }), () => {
            this.setState(state => ({
                userLabel: state.labels.filter(product => product.label == state.userQuizValue),
            }), () => {
                this.setState({
                    labelNameParam : this.state.userLabel.map(o => o.title)
                })
            })
        })
    }

    _renderWheel = (userQuizValue) => {

		switch (userQuizValue) {
			case "R1":
                return <Image source={require('../assets/images/quiz/r1.png')} style={{width: 600, height: 600}} />
            case "R2":
                return <Image source={require('../assets/images/quiz/r2.png')} style={{width: 600, height: 600}} />
            case "R3":
                return <Image source={require('../assets/images/quiz/r3.png')} style={{width: 600, height: 600}} />
            case "R4":
                return <Image source={require('../assets/images/quiz/r4.png')} style={{width: 600, height: 600}} />
            case "R5":
                return <Image source={require('../assets/images/quiz/r5.png')} style={{width: 600, height: 600}} />
            case "B1":
                return <Image source={require('../assets/images/quiz/b1.png')} style={{width: 600, height: 600}} />
            case "B2":
                return <Image source={require('../assets/images/quiz/b2.png')} style={{width: 600, height: 600}} />
            case "B3":
                return <Image source={require('../assets/images/quiz/b3.png')} style={{width: 600, height: 600}} />
            case "B4":
                return <Image source={require('../assets/images/quiz/b4.png')} style={{width: 600, height: 600}} />
		}

	}

    _goToLabel = () => {
        this.props.navigation.navigate({
            routeName: 'Label',
            key: 'result',
            params: {
                label: this.state.labelNameParam,
                from: 'result'
            }
        })
    }

    _renderTitle = () => {
        
        state = this.state;
        let userLabel = state.userLabel

        return (
            userLabel.map((user, index) => {
                return (
                    <View key={index} style={{justifyContent: 'center'}}>
                        <Text style={[styles.result_screen_text, styles.result_screen_white]}>Votre label de prédilection est :</Text>
                        <Text style={[styles.result_screen_label, styles.result_screen_white]}>{user.title} !</Text>
                    </View>
                )
            })
        )
    }

    render() {

        const state = this.state;
        let quizValue = state.userQuizValue;

        return (
            <View style={styles.result_screen_container}>
                <View style={styles.result_screen_container}>
                    <View style={styles.result_screen_left}>
                        <View style={[styles.result_screen_leftBackground, {backgroundColor: colors.Charcoal}]}>
                            { this._renderTitle() }
                            <Button
                                title="En savoir plus"
                                textStyle={styles.result_screen_button_text}
                                buttonStyle={styles.result_screen_button}
                                color={colors.White}
                                onPress={() => this._goToLabel()}
                            />
                        </View>
                    </View>
                    <View style={styles.result_screen_right}>
                        <View style={[styles.result_screen_rightBackground, {backgroundColor: colors.White}]}>
                            { this._renderWheel(quizValue) }
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

export default ResultScreen;

const styles = StyleSheet.create({
    result_screen_container: {
        position: 'relative',
        flex:1,
    },
    result_screen_container: {
        flexDirection: 'row',
        flex:1
    },
    result_screen_left: {
        flex: 1
    },
    result_screen_leftBackground: {
        padding: 40,
        paddingTop: 160,
        height: "100%",
        alignItems: 'center'
    },
    result_screen_right: {
        flex: 2,
        ...Platform.select({
            ios: {
                shadowColor: 'rgba(0,0,0,0.2)',
                shadowOffset: {height: 0, width: 1},
                shadowOpacity: 1,
                shadowRadius: 4
            },
            android: {
                elevation: 1
            }
        })
    },
    result_screen_rightBackground: {
        padding: 40,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'baseline',
        justifyContent: 'space-between',
        flex: 1
    },
    result_screen_white: {
        color: colors.White
    },
    result_screen_button_text: {
        fontFamily: 'markpro-medium',
        fontSize: 18
    },
    result_screen_button: {
        borderWidth: 2,
        borderColor: colors.White,
        backgroundColor: "transparent",
        width: 250,
        height: 56
    },
    result_screen_label: {
        marginBottom: 50,
        width: 230,
        fontSize: 32,
        fontFamily: 'markpro-black',
        lineHeight: 34
    },
    result_screen_text: {
        width: 230,
        marginTop: 10,
        marginBottom: 10,
        fontFamily: 'markpro',
        fontSize: 24,
        lineHeight: 26
    }
});